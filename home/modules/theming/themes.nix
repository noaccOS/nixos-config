{
  bat = {
    dracula = "Dracula";
    nord = "Nord";
    catppuccin = "";
  };
  wezterm = {
    dracula = "TrueDracula";
    nord = "nord";
    catppuccin = "Catppuccin Mocha";
  };
}
